from pathlib import Path
import gradio as gr
import html
import os
import time
import wave
import pyvox

import extensions.silero_tts.script as silero_script
import extensions.silero_tts.tts_preprocessor as silero_preprocessor

from modules import chat, ui_chat
from modules.utils import gradio

try:
    from deep_translator import GoogleTranslator
    dtimport = True
except ImportError:
    print("Couldn't import deep_translator. No translation will be done.")
    dtimport = False

PWD = os.path.dirname(__file__)

params = {
    'activate': True,
    'autoplay': False,
    'voice': None,
    'speaker': None,
    'translate': False,
    "show_text": False,
    "pitch_scale": 0,
    "speed_scale": 1,
    "intonation_scale": 1
}

voices = {}
current_voice = None

def get_available_voices():
    for i in [f.path for f in os.scandir(os.path.join(PWD, "models/")) if f.is_dir()]:
        cf = lambda x: os.path.isfile(os.path.join(i, x))

        n = os.path.basename(i)
        d = lambda x: { "type": x, "directory": i }

        if all({cf("decoder_model.onnx"), cf("embedder_model.onnx"), cf("variance_model.onnx")}):
            yield n, d(pyvox.SharevoxVoiceV2)
        if all({cf("decode.onnx"), cf("predict_duration.onnx"), cf("predict_intonation.onnx")}):
            yield n, d(pyvox.VoicevoxVoice)

def state_modifier(state):
    if not params['activate']:
        return state
    state['stream'] = False
    return state

def history_modifier(history):
    return silero_script.history_modifier(history)

def to_file(wav, name):
    rate = 0
    match voices[params["voice"]]["type"]:
        case pyvox.VoicevoxVoice:
            rate = 24000
        case pyvox.SharevoxVoiceV2:
            rate = 48000

    path = Path(os.path.join(PWD, f"outputs/{name}")).as_posix()
    print("Output: " + path)

    with wave.open(path, 'w') as f:
        f.setnchannels(1)
        f.setsampwidth(2)
        f.setframerate(rate)
        f.writeframes((wav * (2 ** 15 - 1)).astype("<h"))

    return path

def voice_preview():
    if current_voice and params["speaker"] is not None:
        text = "日本語文を解析し、音声合成エンジンに渡せる形式に変換します．"
        wav = current_voice.run(text, params["speaker"])
        path = to_file(wav, "sample.wav")
        return f'<audio src="file/{path}?{int(time.time())}" controls autoplay></audio>'

def setup():
    global voices
    voices = dict(get_available_voices())
    if params["voice"]:
        select_voice(params["voice"])

def update_pitch_scale():
    if current_voice:
        current_voice.pitch_scale = params["pitch_scale"]

def update_speed_scale():
    if current_voice:
        current_voice.speed_scale = params["speed_scale"]

def update_intonation_scale():
    if current_voice:
        current_voice.intonation_scale = params["intonation_scale"]

def select_voice(voice):
    params.update({"voice": voice})
    v = voices[voice]
    global current_voice
    current_voice = v["type"](v["directory"])

    update_pitch_scale()
    update_speed_scale()
    update_intonation_scale()

def output_modifier(string, state):
    if not current_voice or params["speaker"] is None or not params['activate']:
        return string
    if string == '':
        return '*Empty reply, try regenerating*'
    if (vstr := silero_preprocessor.preprocess(html.unescape(string).lower())) == '':
        return string
    if dtimport and params['translate']:
        vstr = GoogleTranslator(target='ja').translate(vstr)
    wav = current_voice.run(vstr, params["speaker"])
    path = to_file(wav, f"{int(time.time())}.wav")
    out = f'<audio src="file/{path}" controls {"autoplay" if params["autoplay"] else ""}></audio>'
    return out + f"\n\n{string}" if params["show_text"] else ""

def ui():
    with gr.Accordion("Pyvox TTS"):
        with gr.Row():
            activate = gr.Checkbox(value=params['activate'], label='Activate TTS')
            autoplay = gr.Checkbox(value=params['autoplay'], label='Play TTS automatically')
            translate = gr.Checkbox(value=params['translate'], label='Translate voice to Japanese')
            show_text = gr.Checkbox(value=params['show_text'], label='Show message text under audio player')

        with gr.Row():
            voice = gr.Dropdown(
                    value=params['voice'],
                    choices=voices.keys(),
                    interactive=True,
                    label='TTS Voice')
            speaker = gr.Number(
                    precision=0,
                    value=params['speaker'],
                    interactive=True,
                    label='Speaker ID')
            preview_play = gr.Button("Preview")
            preview_audio = gr.HTML(visible=False)

        with gr.Row():
            pitch_scale = gr.Number(
                    value=params['pitch_scale'],
                    interactive=True,
                    label='Pitch scale')
            speed_scale = gr.Number(
                    value=params['speed_scale'],
                    interactive=True,
                    label='Speed scale')
            intonation_scale = gr.Number(
                    value=params['intonation_scale'],
                    interactive=True,
                    label='Intonation scale')

    activate.change(lambda x: params.update({"activate": x}), activate, None)
    autoplay.change(lambda x: params.update({"autoplay": x}), autoplay, None)
    translate.change(lambda x: params.update({"translate": x}), translate, None)

    show_text.change(
        lambda x: params.update({"show_text": x}), show_text, None).then(
        silero_script.toggle_text_in_history, gradio('history'), gradio('history')).then(
        chat.save_history, gradio('history', 'unique_id', 'character_menu', 'mode'), None).then(
        chat.redraw_html, gradio(ui_chat.reload_arr), gradio('display'))

    voice.change(select_voice, voice, speaker)
    speaker.change(lambda x: params.update({"speaker": x}), speaker, None)
    preview_play.click(voice_preview, None, preview_audio)

    pitch_scale.change(lambda x: params.update({"pitch_scale": x}), pitch_scale, None).then(update_pitch_scale)
    speed_scale.change(lambda x: params.update({"speed_scale": x}), speed_scale, None).then(update_speed_scale)
    intonation_scale.change(lambda x: params.update({"intonation_scale": x}), intonation_scale, None).then(update_intonation_scale)
